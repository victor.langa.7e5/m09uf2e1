﻿namespace M09UF2E1
{
    internal class Prova
    {
        const int _howManyThreads = 3;
        string _filesPath = @"..\..\..\files\";
        string[] _files = new string[]
        {
            "RazonesPorLasQueSoyUnBuenProgramador.txt",
            "JuegosQueMeParecenLaPolla.txt",
            "ManifiestoComunista.txt"
        };

        /*
         * Archivos disponibles:
         *      - RazonesPorLasQueSoyUnBuenProgramador.txt
         *      - Mondongo.txt
         *      - JuegosQueMeParecenLaPolla.txt
         *      - TheBusiness_Tiesto.txt
         *      - JuegosQueTengo.txt
         *      - ManifiestoComunista.txt
        */

        public void CallThreads()
        {
            for (int i = 0; i < _howManyThreads; i++)
                StartCountLinesThread(_files[i]);
        }

        void StartCountLinesThread(string file) => new Thread(() => CountHowManyLines(file)).Start();

        void CountHowManyLines(string file) => Console.WriteLine("File {0}: {1} lines", file, File.ReadAllLines(_filesPath + file).Length);
    }
}
